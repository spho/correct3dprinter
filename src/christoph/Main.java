package christoph;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class Main {

	static ConfigData conf;
	static String inputFileName = "input.gcode";
	static String ouputFileName = "output.gcode";
	static String configFileName = "config.txt";

	public static void main(String[] args) {
		  if (args.length > 2) {
			inputFileName = args[0];
			ouputFileName = args[1];
			configFileName = args[2];
		}else if (args.length > 1) {
			inputFileName = args[0];
			ouputFileName = args[1];
		}else if (args.length > 0) {
			inputFileName = args[0];
		}

		// Load Config
		conf = new ConfigData(configFileName, 0);
		if (conf.validData == false) {
			System.out.println("Error while reading the config file");
			System.exit(-2);
		}
		System.out.println("Loaded: "+configFileName+ " as config file.");

		if (transform(inputFileName, ouputFileName) == false) {
			System.out.println("Error whil transforming file");
			System.exit(-3);
		}
		System.out.println("Transformed: "+inputFileName+ " and stored new file as; "+ouputFileName);

	}
	

	private static boolean transform(String InFileName, String OutFilename) {

		try {
			BufferedReader br = new BufferedReader(new FileReader(InFileName));
			BufferedWriter bw = new BufferedWriter(new FileWriter(OutFilename));

			String lineBuf = "";
			String outBuf = "";
			lineBuf = br.readLine();

			double x0 = -1;
			double y0 = -1;

			while (lineBuf != null) {
				lineBuf = lineBuf+"  ";
				if (lineBuf.length() > 2 && (lineBuf.startsWith("G1")||lineBuf.startsWith("G0"))) {
					//System.out.println("G1 detected");

					
					int xpos = lineBuf.lastIndexOf("X");
					int ypos =lineBuf.lastIndexOf("Y");
					String rest = "";
					String front = "";
					double x = 0;
					double y = 0;

					if (xpos != -1 && ypos != -1) {
						for (int i = xpos; i < lineBuf.length() - 1 && x == 0; i++) {
							if (lineBuf.charAt(i) == ' ') {
								x = Double.parseDouble(lineBuf.substring(xpos + 1, i));
								front= lineBuf.substring(0, xpos);
								if (x0 == -1) {
									x0 = x;
								}
							}
						}
						for (int i = ypos; i < lineBuf.length() - 1 && y == 0; i++) {
							if (lineBuf.charAt(i) == ' ') {
								y = Double.parseDouble(lineBuf.substring(ypos + 1, i));
								if (y0 == -1) {
									y0 = y;
								}
								rest = lineBuf.substring(i);
							}
						}

						x = Math.floor((x - (y - y0) * Math.tan(conf.rotationMismatch)) * 1000) / 1000;
						String xStr = Double.toString(x);
						int xExpectedLength = 7;
						if (x < 100) {
							xExpectedLength--;
						}
						if (x < 10) {
							xExpectedLength--;
						}

						for (int i = 0; i < xExpectedLength - xStr.length(); i++) {
							xStr += "0";
						}

						int yExpectedLength = 7;
						if (y < 100) {
							yExpectedLength--;
						}
						if (y < 10) {
							yExpectedLength--;
						}
						y = Math.floor((y0 + (y - y0) / Math.cos(conf.rotationMismatch)) * 1000) / 1000;
						String yStr = Double.toString(y);
						for (int i = 0; i < yExpectedLength - yStr.length(); i++) {
							yStr += "0";
						}

						outBuf = front +"X"+ xStr + " Y" + yStr + rest;

					} else {
						outBuf = lineBuf;
					}

				} else {
					outBuf = lineBuf;
				}
				bw.write(outBuf + System.lineSeparator());
				//bw.flush();
				lineBuf = br.readLine();
			}
			br.close();
			bw.close();
		} catch (Exception e) {
			return false;
		}

		return true;
	}
}
