package christoph;

import java.io.BufferedReader;
import java.io.FileReader;

public class ConfigData {

	double x = 1;
	double y = 1;
	double dx = 0;
	double rotationMismatch = 0;
	double globalRotation = 0;

	boolean validData = false;

	public ConfigData(String configFilePath, int mode) {

		try {
			BufferedReader br = new BufferedReader(new FileReader(configFilePath));
		
			br.readLine();
			br.readLine();
			br.readLine();
			x = Double.parseDouble(br.readLine());
			br.readLine();
			br.readLine();
			y = Double.parseDouble(br.readLine());
			br.readLine();
			br.readLine();
			dx = Double.parseDouble(br.readLine());
			br.readLine();
			br.readLine();
			rotationMismatch = Double.parseDouble(br.readLine());
			br.readLine();
			br.readLine();
			globalRotation = Double.parseDouble(br.readLine());

			br.close();
			validData = true;

		} catch (Exception e) {
			validData = false;
		}
		if(mode == 0){
			//Load all values from the config file
			
		}else if(mode == 1){
			//Calculate rotation mismacht form x,y,dx value
			rotationMismatch = Math.atan(dx/y);
			
		}else{
			validData = false;
		}
		
	}

}
